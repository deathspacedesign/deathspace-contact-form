import React, { Component } from "react"
import "../scss/loader.scss"
import gsap, { TweenMax, TimelineMax } from "gsap"
import tick from "../../static/tick.svg"

export default class Contact extends Component {
  constructor() {
    super()
    this.state = {
      name: "",
      email: "",
      message: "",
      submitText: "",
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    TweenMax.set(".headerText", {
      opacity: 0,
    })
    TweenMax.set(".demo", {
      opacity: 0,
    })
  }

  handleChange(e) {
    const target = e.target
    const name = target.name
    const value = target.value
    this.setState({
      [name]: value,
    })
  }

  handleSubmit(e) {
    const tl = new TimelineMax({ paused: true })
    tl.fromTo(
      ".headerText",
      { duration: 1, opacity: 0, y: "16px" },
      { duration: 1, opacity: 1, y: 0 }
    )
    const tl2 = new TimelineMax({ paused: true })
    tl2.fromTo(
      ".demo",
      { duration: 1, opacity: 0 },
      { duration: 1, opacity: 1 }
    )

    e.preventDefault()
    tl2.play()
    this.setState({
      submitText: "⌛ Submitting your form...",
    })

    const submitName = this.state.name
    const submitEmail = this.state.email
    const submitMessage = this.state.message

    fetch(
      "https://cors-anywhere.herokuapp.com/https://formsubmit.co/ajax/hari8697@altmails.com",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: submitName,
          email: submitEmail,
          message: submitMessage,
        }),
      }
    )
      .then(resp => {
        console.log(resp)
        this.setState({
          submitText: "🎉 Success",
        })
        tl2.reverse()
        setTimeout(() => {
          tl.play()
        }, 300)
        return resp.json()
      })
      .then(user => {
        console.log(user)
      })
      .catch(err => {
        console.log(err)
        this.setState({
          submitText: "❌ Failed to submit.",
        })
      })

    setTimeout(() => {
      tl.reverse()
    }, 4000)

    setTimeout(() => {
      this.setState({
        submitText: "",
      })
    }, 5000)
  }

  render() {
    return (
      <div className="s6">
        <form
          onSubmit={this.handleSubmit}
          // action="https://cors-anywhere.herokuapp.com/https://formsubmit.co/b8762b1802caefca05176724d8868c69"
          // method="POST"
        >
          <h1 className="title">Get in touch</h1>
          <div className="input">
            <input
              type="text"
              name="name"
              data-validate-field="name"
              placeholder="Name"
              id="name-field"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </div>

          <div className="input">
            <input
              type="text"
              name="email"
              data-validate-field="email"
              placeholder="Your email address"
              id="email-field"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>

          <textarea
            name="message"
            cols="30"
            rows="10"
            placeholder="Message"
            className="message-field"
            value={this.state.message}
            onChange={this.handleChange}
          ></textarea>

          <input type="hidden" name="_template" value="table" />
          <input type="hidden" name="_captcha" value="false" />
          <input type="hidden" name="_next" value="http://localhost:8000/" />
          <input type="submit" value="Submit" className="submit-btn" />

          <p className="thanks">{this.state.submitText}</p>

          <div className="animation">
            <div className="masker">
              <img src={tick} className="headerText" />
            </div>
            <div className="demo">
              <div className="circle">
                <div className="inner"></div>
              </div>
              <div className="circle">
                <div className="inner"></div>
              </div>
              <div className="circle">
                <div className="inner"></div>
              </div>
              <div className="circle">
                <div className="inner"></div>
              </div>
              <div className="circle">
                <div className="inner"></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

// <div className="text">
//           <h2>{this.state.name}</h2>
//           <h2>{this.state.email}</h2>
//           <h2>{this.state.message}</h2>
//         </div>
